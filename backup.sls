backup_directory:
  file.directory:
    - name: /home/backup
    - user: root
    - group: root
    - mode: 700
    - makedirs: True

deploy_backup_script:
  file.managed:
    - name: /usr/local/bin/backup_script.sh
    - source: salt://wordpress/backup_script.sh
    - user: root
    - mode: 700

schedule_cron_backup:
  cron.present:
    - name: "/usr/local/bin/backup_script.sh >> /var/log/backup_script.log 2>&1"
    - user: root
    - minute: '0'
    - hour: '2'
    - daymonth: '*'
    - month: '*'
    - dayweek: '*'
    - identifier: "backup_script"
    - comment: "Daily backup script"

