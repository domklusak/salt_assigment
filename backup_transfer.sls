include:
  - wordpress.backup

sync_backup_to_master:
  module.run:
    - name: rsync.rsync
    - src: /home/backup/
    - dst: root@195.210.29.158:/home/db_backups
    - delete: True
    - opts:
      - '-avz'
      - '--exclude="*.tmp"'
    - require:
      - cron: schedule_cron_backup

