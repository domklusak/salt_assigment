mysql_server:
  pkg.installed:
    - name: mysql-server

mysql_service:
  service.running:
    - name: mysql
    - enable: True
    - require:
      - pkg: mysql_server

mysql_db:
  mysql_database.present:
    - name: wordpress
    - require:
      - service: mysql_service

mysql_user:
  mysql_user.present:
    - name: wpuser
    - host: localhost
    - password: 'heslojekreslo'
    - require:
      - service: mysql_service

mysql_grants:
  mysql_grants.present:
    - grant: all privileges
    - database: wordpress.*
    - user: wpuser
    - host: localhost

