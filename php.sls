php_packages:
  pkg.installed:
    - pkgs:
      - php
      - libapache2-mod-php
      - php-mysql
      - php-curl
      - php-gd
      - php-xml
      - php-mbstring
      - php-zip
    - require:
      - pkg: install_apache

