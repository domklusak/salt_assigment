install_apache:
  pkg.installed:
    - name: apache2

apache_service:
  service.running:
    - name: apache2
    - enable: True
    - require:
      - pkg: install_apache

apache_site_config:
  file.managed:
    - name: /etc/apache2/sites-available/wordpress.conf
    - source: salt://wordpress/wordpress.conf
    - require:
      - pkg: install_apache

enable_site:
  cmd.run:
    - name: a2ensite wordpress.conf && a2dissite 000-default.conf && a2enmod rewrite && systemctl reload apache2
    - require:
      - file: apache_site_config


