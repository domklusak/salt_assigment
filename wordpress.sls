ensure_directory:
  file.directory:
    - name: /var/www/html/project
    - user: www-data
    - group: www-data
    - mode: 755
    - makedirs: True

download_wordpress:
  cmd.run:
    - name: 'wget https://wordpress.org/latest.tar.gz -O /tmp/wordpress.tar.gz && tar -xzvf /tmp/wordpress.tar.gz -C /var/www/html/project/ && rm /tmp/wordpress.tar.gz'
    - require:
      - file: ensure_directory

set_permissions:
  file.directory:
    - name: /var/www/html/project/wordpress
    - user: www-data
    - group: www-data
    - recurse:
      - user
      - group
      - mode
    - dir_mode: 755
    - file_mode: 644
    - require:
      - cmd: download_wordpress

