## VPS Configurations
### Master
- Name: dominikmaster
- Salt States Directory: `/srv/salt/wordpress`
- Backup Directory synced from minion: `/home/backup`
- Zabbix Backup Check Script: `/usr/local/bin/backup_check.sh`
### Minion
- Name: dominikminion
- Project Directory: `/var/www/html/project/wordpress/`
- Salted backup Script: `/usr/local/bin/backup_check.sh`
- Backup directory: `/home/backup`
- WordPress Configuration Check Script for zabbix: `/usr/local/bin/check_wp_config.sh`

## Files in repository

- `apache.sls`: Salt state file for configuring Apache.
- `backup.sls`: Salt state file for setting up backups.
- `backup_script.sh`: Script for performing the database backup.
- `backup_transfer.sls`: Salt state file for transferring backups to the master.
- `init.sls`: Initial Salt state file.
- `mysql.sls`: Salt state file for configuring MySQL.
- `php.sls`: Salt state file for configuring PHP.
- `wordpress.conf`: Configuration file for WordPress.
- `wordpress.sls`: Salt state file for setting up WordPress.
